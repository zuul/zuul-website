. display in 68x24
.. display in 88x24

.. pygments yaml? (only file breaks (---) tinted)
.. slide on high level v3 changes
.. slide on nodepool

.. transition:: dissolve
   :duration: 0.4

Test Slide
==========
.. hidetitle::

.. ansi:: images/testslide.ans

Preshow
=======
.. hidetitle::

.. ansi:: images/cursor.ans images/cursor2.ans

Zuul
====
.. hidetitle::
.. ansi:: images/zuul.ans

Since Berlin (3.3.0)
====================

* 8 releases (3.3.1, 3.4.0, 3.5.0, 3.6.0, 3.6.1, 3.7.0, 3.7.1, 3.8.0)
* Jobs can control which child jobs run
* Artifact URLs are stored along with job results and passed to child jobs
* Region-local executors (to support complex multi-cloud deployments
* Easier to share jobs which use secrets
* AWS and OpenShift support
* Support for speculative container execution

Speculative Container Execution
===============================

* See Jim's Keynote Tomorrow
* Applying multi-project speculative exection to container layers

Specs Landed and Implemented
============================

* Kubernetes Build Resources
* Multiple Ansible Versions

OpenDev Zuul Rollout
====================

* https://zuul.opendev.org
* Multi-tenant deployment
* Zuul is now at https://opendev.org/zuul

Zuul For Ansible
================

* Deployment by Ansible of a Zuul for Ansible Projects
* https://dashboard.zuul.ansible.com/tenants

Specs In Review
===============

* Web Dashboard Log Handling https://review.opendev.org/#/c/648714
* Tenant-scoped admin web API https://review.opendev.org/#/c/562321
* Scale out scheduler (WIP) https://review.opendev.org/#/c/621479

In Progress / Expected
======================

* Github Checks API and in-line comments parity
* Pagure (interest in Pagure, Bitbucket)
* GCE/Azure
* Kubernetes Operator
